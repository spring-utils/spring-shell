package springshell.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import springshell.config.ShellConfiguration;

@ShellComponent
public class CommonCommands {

    @Autowired
    private ShellConfiguration configuration;

    @ShellMethod("View app version.")
    public String showVersion() {
        return configuration.getVersion();
    }
}
