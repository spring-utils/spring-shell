package springshell.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class ShellConfiguration {

    @Value("${shell.version}")
    private String version;

}
